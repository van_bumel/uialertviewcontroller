//
//  SecondViewController.h
//  UIAlertViewController
//
//  Created by OnSight MacBook Pro on 6/7/18.
//  Copyright © 2018 OnSight MacBook Pro. All rights reserved.
//

#import "ViewController.h"

@interface SecondViewController : ViewController

- (void) configureViewWithData: (NSString*) data;


@end

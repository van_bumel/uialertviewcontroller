//
//  SecondViewController.m
//  UIAlertViewController
//
//  Created by OnSight MacBook Pro on 6/7/18.
//  Copyright © 2018 OnSight MacBook Pro. All rights reserved.
//

#import "SecondViewController.h"

//Helpers
#import "AlertsFactory.h"

@interface SecondViewController ()

//properties
@property (weak, nonatomic) IBOutlet UILabel* enteredTextLabel;
@property (weak, nonatomic) IBOutlet UILabel* textFromPreviousController;

@property (strong, nonatomic) NSString* content;

//methods
- (IBAction) onShowFirstAlertButtonPressed: (UIButton*) sender;
- (IBAction) onShowSecondAlertPressed: (UIButton*) sender;
- (IBAction) onShowThirdAlertButtonPressed: (UIButton*) sender;
- (IBAction) onBackButtonPressed: (UIButton*) sender;

@end

@implementation SecondViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.textFromPreviousController.text = self.content;
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Public methods -

- (void) configureViewWithData: (NSString*) data
{
    self.content = data;
}


#pragma mark - Actions -

- (IBAction) onShowFirstAlertButtonPressed: (UIButton*) sender
{
    [AlertsFactory showAlertControllerWithTitle: @"First title"
                                    withMessage: @"First alert message"
                        presentedFromController: self
                                 withCompletion: ^(AlertActionType actionType) {
                                     
                                 }];
}

- (IBAction) onShowSecondAlertPressed: (UIButton*) sender
{
    NSArray* arrayWithActionTitiles = @[@"First action", @"Second action", @"Third action"];
    
    [AlertsFactory showAlertControllerWithTitle: @"Second title"
                                    withMessage: @"Second alert message"
                              withActionsTitles: arrayWithActionTitiles
                        presentedFromController: self
                            withCompletionBlock: ^(NSUInteger actionIndex) {
                                
                                if (actionIndex == 0)
                                {
                                    // Прописываем логику для первого экшена
                                }
                                else
                                    if ( actionIndex == 1)
                                {
                                    // Прописываем логику для первого экшена и так далее
                                }
                                
                            }];
}

- (IBAction) onShowThirdAlertButtonPressed: (UIButton*) sender
{
    [AlertsFactory showAlertWithTextFieldWithTitle: @"Third title"
                                       withMessage: @"Third alert message"
                                 withActionsTitles: @[@"Save"]
                               withPlaceholderText: @"Enter text"
                           presentedFromController: self
                               withCompletionBlock: ^(NSUInteger actionIndex, NSString* enteredText) {
                                   
                                   // Здусь прописываем логику для экшенов
                                   if ( actionIndex == 0)
                                   {
                                       // А так же прописываем логику для введенного текста
                                       self.enteredTextLabel.text = enteredText;
                                   }
                               }];
}

- (IBAction) onBackButtonPressed: (UIButton*) sender
{
    [self.navigationController popViewControllerAnimated: YES];
}

@end

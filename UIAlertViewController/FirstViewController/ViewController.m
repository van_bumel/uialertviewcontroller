//
//  ViewController.m
//  UIAlertViewController
//
//  Created by OnSight MacBook Pro on 6/7/18.
//  Copyright © 2018 OnSight MacBook Pro. All rights reserved.
//

#import "ViewController.h"

//Classes
#import "SecondViewController.h"

@interface ViewController ()

//properties
@property (weak, nonatomic) IBOutlet UILabel* nameLabel;
@property (weak, nonatomic) IBOutlet UILabel* passwordLable;

//methods
- (IBAction) onSimpleAlertButtonPressed: (UIButton*) sender;
- (IBAction) onAlertWithOkButtonPressed: (UIButton*) sender;
- (IBAction) onAlertWithTextFieldButtonPressed: (UIButton*) sender;

@end

@implementation ViewController

#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Actions -

- (IBAction) onSimpleAlertButtonPressed: (UIButton*) sender
{
    [self createSimpleAlert];
}

- (IBAction) onAlertWithOkButtonPressed: (UIButton*) sender
{
    [self simpleAlertWithAlert: @"Go to second screen"
                   withMessage: @"Переход состоится по нажатию на кнопу ОК"];
}

- (IBAction) onAlertWithTextFieldButtonPressed: (UIButton*) sender
{
    [self createAlertWithTextFields];
}


#pragma mark - Segues -

- (void) prepareForSegue: (UIStoryboardSegue*) segue
                  sender: (id)                 sender
{
    [super prepareForSegue: segue
                    sender: sender];
    
    if ( [segue.identifier isEqualToString: @"ShowSecondViewControllerID"] )
    {
        SecondViewController* secondController = segue.destinationViewController;
        
        [secondController configureViewWithData: self.passwordLable.text];
    }
}


#pragma mark - Internal methods -

- (void) createSimpleAlert
{
    // Создаем сам алерт контроллер, даем ему его тайтл и сообщение которое он должен отобразить, так же показать стиль в котором он должен отобразится
    
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle: @"Simple Alert"
                                                                             message: @"This is simple alert"
                                                                      preferredStyle: UIAlertControllerStyleAlert];
    
    // Создаем кнопки которые должны отображаться на алерте
    
    UIAlertAction* yesButtonAction = [UIAlertAction actionWithTitle: @"Yes"
                                                        style: UIAlertActionStyleDefault
                                                      handler: ^(UIAlertAction * _Nonnull action) {
                                                         
                                                          // здесь прописываем логику на выполнение экшена, по которому мы хотим что то выполнить
                                                          
                                                      }];
    
    UIAlertAction* noButtonAction = [UIAlertAction actionWithTitle: @"NO"
                                                             style: UIAlertActionStyleDefault
                                                           handler: ^(UIAlertAction * _Nonnull action) {
                                                               
                                                               // так же прописываем логику на выполнение по экшену "NO"
                                                           }];
    
    // добавляем созданные экшены алерту
    [alertController addAction: noButtonAction];
    [alertController addAction: yesButtonAction];
    
    // метод для презентации алерта на контроллере
    [self presentViewController: alertController
                       animated: YES
                     completion: nil];
}

- (void) simpleAlertWithAlert: (NSString*) title
                  withMessage: (NSString*) message
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle: title
                                                                             message: message
                                                                      preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction* alertAction = [UIAlertAction actionWithTitle: @"OK"
                                                          style: UIAlertActionStyleDefault
                                                        handler: ^(UIAlertAction * _Nonnull action) {
                                                            
                                                            [self performSegueWithIdentifier: @"ShowSecondViewControllerID"
                                                                                      sender: action];
                                                            
                                                        }];
    
    [alertController addAction: alertAction];
    
    [self presentViewController: alertController
                       animated: YES
                     completion: nil];
}

- (void) createAlertWithTextFields
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle: @"Login"
                                                                             message: @"Input username and password"
                                                                      preferredStyle: UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler: ^(UITextField * _Nonnull textField) {
        
        textField.placeholder     = @"Enter name";
        textField.textColor       = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle     = UITextBorderStyleRoundedRect;
    }];
    
    [alertController addTextFieldWithConfigurationHandler: ^(UITextField * _Nonnull textField) {
        
        textField.placeholder     = @"password";
        textField.textColor       = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle     = UITextBorderStyleRoundedRect;
        textField.secureTextEntry = YES;
    }];
    
    UIAlertAction* alertAction = [UIAlertAction actionWithTitle: @"OK"
                                                          style: UIAlertActionStyleDefault
                                                        handler: ^(UIAlertAction *action) {
                                                            
                                                            NSArray* textFields        = alertController.textFields;
                                                            UITextField* nameField     = textFields[0];
                                                            UITextField* passwordFiled = textFields[1];
                                                            
                                                            self.nameLabel.text     = nameField.text;
                                                            self.passwordLable.text = passwordFiled.text;
                                                        }];
    
    [alertController addAction: alertAction];
    
    [self presentViewController: alertController
                       animated: YES
                     completion: nil];
}


@end















//
//  AlertsFactory.m
//  ViperSampleProject
//
//  Created by Nikolay Chaban on 3/24/17.
//  Copyright © 2017 Nikolay Chaban. All rights reserved.
//

#import "AlertsFactory.h"
#import "UIAlertController+Window.h"

typedef void(^AlertActionBlock)(UIAlertAction* action);

@implementation AlertsFactory


#pragma mark - Public methods -

+ (void) showAlertControllerWithTitle: (NSString*)            title
                          withMessage: (NSString*)            message
              presentedFromController: (UIViewController*)    controller
                       withCompletion: (AlertCompletionBlock) completion
{
    UIAlertController* alertController = [AlertsFactory createAlertControllerWithTitle: title
                                                                           withMessage: message
                                                                                 style: UIAlertControllerStyleAlert];
    
    UIAlertAction* closeAction = [UIAlertAction actionWithTitle: @"Confirm"
                                                          style: UIAlertActionStyleDefault
                                                        handler: ^(UIAlertAction * _Nonnull action) {
                                                        
                                                            if ( completion )
                                                                completion(ConfirmAlertActionType);
                                                            
                                                        }];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle: @"Cancel"
                                                           style: UIAlertActionStyleDefault
                                                         handler: ^(UIAlertAction * _Nonnull action) {
                                                            
                                                            if ( completion )
                                                                completion(CancelAlertActionType);
                                                            
                                                        }];
    
    [alertController addAction: closeAction];
    [alertController addAction: cancelAction];
    
    [controller presentViewController: alertController
                             animated: YES
                           completion: nil];
}

+ (void) showInformAlertControllerWithTitle: (NSString*)         title
                                withMessage: (NSString*)         message
                             fromController: (UIViewController*) source
{
    UIAlertController* alertController = [AlertsFactory createAlertControllerWithTitle: title
                                                                           withMessage: message
                                                                                 style: UIAlertControllerStyleAlert];
    
    UIAlertAction* closeAction = [UIAlertAction actionWithTitle: @"Confirm"
                                                          style: UIAlertActionStyleDefault
                                                        handler: nil];
    
    [alertController addAction: closeAction];
    
    [source presentViewController: alertController
                         animated: YES
                       completion: nil];
}

+ (void) showOnTopInformAlertControllerWithTitle: (NSString*) title
                                     withMessage: (NSString*) message
{
    UIAlertController* alertController = [AlertsFactory createAlertControllerWithTitle: title
                                                                           withMessage: message
                                                                                 style: UIAlertControllerStyleAlert];
    
    UIAlertAction* closeAction = [UIAlertAction actionWithTitle: @"Confirm"
                                                          style: UIAlertActionStyleDefault
                                                        handler: nil];
    
    [alertController addAction: closeAction];
    
    [alertController show];
}

+ (void) showInformAlertControllerWithTitle: (NSString*)             title
                                withMessage: (NSString*)             message
                             fromController: (UIViewController*)     source
                             withCompletion: (CompletionWithSuccess) completion
{
    UIAlertController* alertController = [AlertsFactory createAlertControllerWithTitle: title
                                                                           withMessage: message
                                                                                 style: UIAlertControllerStyleAlert];
    
    UIAlertAction* closeAction = [UIAlertAction actionWithTitle: @"Confirm"
                                                          style: UIAlertActionStyleDefault
                                                        handler: ^(UIAlertAction * _Nonnull action) {
                                                            
                                                            if (completion) {
                                                                completion(YES);
                                                            }
                                                        }];
    
    [alertController addAction: closeAction];
    
    [source presentViewController: alertController
                         animated: YES
                       completion: nil];
}


+ (void) showAlertControllerWithTitle: (NSString*)                   title
                          withMessage: (NSString*)                   message
                    withActionsTitles: (NSArray*)                    titles
              presentedFromController: (UIViewController*)           controller
                  withCompletionBlock: (AlertActionsCompletionBlock) completion
{
    UIAlertController* alertController = [self createAlertControllerWithTitle: title
                                                                  withMessage: message
                                                                        style: UIAlertControllerStyleAlert];
    
 
    AlertActionBlock actionsBlock = ^(UIAlertAction* action){
        
        /**
         @author Nikolay Chaban
         
         Index of pressed button geting with using action title and all titles array
         */
        if ( completion )
            completion([titles indexOfObject: action.title]);
        
    };
    
    NSArray<UIAlertAction*>* actions = [self createAlertActionsWithTitles: titles
                                                          withActionstyle: UIAlertActionStyleDefault
                                                          withActionBlock: actionsBlock];
   
    [actions enumerateObjectsUsingBlock: ^(UIAlertAction * _Nonnull action, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [alertController addAction: action];
    }];
    
    [controller presentViewController: alertController
                             animated: YES
                           completion: nil];
}

+ (void) showAlertWithTextFieldWithTitle: (NSString*)                           title
                             withMessage: (NSString*)                           message
                       withActionsTitles: (NSArray*)                            titles
                     withPlaceholderText: (NSString*)                           placeholderText
                 presentedFromController: (UIViewController*)                   controller
                     withCompletionBlock: (AlertActionsWithTextCompletionBlock) completion
{
    UIAlertController* alertController = [self createAlertControllerWithTitle: title
                                                                  withMessage: message
                                                                        style: UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler: ^(UITextField * _Nonnull textField) {
        
        textField.placeholder = placeholderText;
    }];
    
    UITextField* alertTextField = alertController.textFields.firstObject;
    
    AlertActionBlock actionsBlock = ^(UIAlertAction* action){
        
        if ( completion )
            completion([titles indexOfObject: action.title], alertTextField.text);
        
    };
    
    NSArray<UIAlertAction*>* actions = [self createAlertActionsWithTitles: titles
                                                          withActionstyle: UIAlertActionStyleDefault
                                                          withActionBlock: actionsBlock];
    
    
    [actions enumerateObjectsUsingBlock: ^(UIAlertAction * _Nonnull action, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [alertController addAction: action];
    }];
    
    [controller presentViewController: alertController
                             animated: YES
                           completion: nil];
}

+ (void) showActionSheetWithTitle: (NSString*)                   title
                          message: (NSString*)                   message
                withActionsTitles: (NSArray*)                    titles
          presentedFromController: (UIViewController*)           controller
              withCompletionBlock: (AlertActionsCompletionBlock) completion
{
    UIAlertController* alert = [self createAlertControllerWithTitle: title
                                                        withMessage: message
                                                              style: UIAlertControllerStyleActionSheet];
    
    AlertActionBlock actionsBlock = ^(UIAlertAction* action){
        
        /**
         @author Nikolay Chaban
         
         Index of pressed button geting with using action title and all titles array
         */
        if ( completion )
            completion([titles indexOfObject: action.title]);
        
    };
    
    NSArray<UIAlertAction*>* actions = [self createAlertActionsWithTitles: titles
                                                          withActionstyle: UIAlertActionStyleDestructive
                                                          withActionBlock: actionsBlock];
    
    [actions enumerateObjectsUsingBlock: ^(UIAlertAction * _Nonnull action, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [alert addAction: action];
    }];
    
    [controller presentViewController: alert
                             animated: YES
                           completion: nil];
}


#pragma mark - Internal methods -

+ (UIAlertController*) createAlertControllerWithTitle: (NSString*)              title
                                          withMessage: (NSString*)              message
                                                style: (UIAlertControllerStyle) style
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle: title
                                                                             message: message
                                                                      preferredStyle: style];
    
    return alertController;
}

+ (NSArray<UIAlertAction*>*) createAlertActionsWithTitles: (NSArray*)           titles
                                          withActionstyle: (UIAlertActionStyle) actionStyle
                                          withActionBlock: (AlertActionBlock)   block
{
    __block NSMutableArray* actionsArr = [NSMutableArray arrayWithCapacity: titles.count];
    
    // Create last action for hiding alert with cancel type
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle: @"Cancel"
                                                           style: UIAlertActionStyleCancel
                                                         handler: block];
    
    [actionsArr addObject: cancelAction];
    
    // Create alert actions for each item from titles array
    [titles enumerateObjectsUsingBlock: ^(NSString*  _Nonnull title, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UIAlertAction* action = [UIAlertAction actionWithTitle: title
                                                         style: actionStyle
                                                       handler: block];
        
        [actionsArr addObject: action];
        
    }];
    
    return actionsArr;
}

@end

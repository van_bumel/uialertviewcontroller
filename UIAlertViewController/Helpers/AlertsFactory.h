//
//  AlertsFactory.h
//  ViperSampleProject
//
//  Created by Nikolay Chaban on 3/24/17.
//  Copyright © 2017 Nikolay Chaban. All rights reserved.
//

// Classes
@class UIAlertController;
@class UIViewController;

// Constants
#import "ConstantsBlock.h"

///-------------------------------------------------
/// @name Alert action type for detectin callback action
///-------------------------------------------------
typedef NS_ENUM(NSUInteger, AlertActionType)
{
    ConfirmAlertActionType,
    CancelAlertActionType,
};

typedef NS_ENUM(NSUInteger, AlertPDFActionType)
{
    OpenAsPDFType        = 0,
    SendEmailWithPDFType = 1,
};

typedef NS_ENUM(NSUInteger, ShapeType) {
    kCircle,
    kRectangle,
    kOblateSpheroid
};

///-------------------------------------------------
/// @name Alert action call back block
///-------------------------------------------------
typedef void(^AlertCompletionBlock)(AlertActionType actionType);

typedef void(^AlertActionsCompletionBlock)(NSUInteger actionIndex);

typedef void(^AlertActionsWithTextCompletionBlock)(NSUInteger actionIndex, NSString* enteredText);

/**
 @author Nikolay Chaban
 
 Factory designed for setuping and presenting alerts from View layer
 */
@interface AlertsFactory : NSObject

/**
 @author Nikolay Chaban
 
 Method designed for initialized and presenting alert controller on view layer(controller) 
 with needed title and message

 @param title alert title string value
 @param message message inside alert string value
 @param controller controller which will present alert controller
 @param completion completion block, if need to get callback from action
 */
+ (void) showAlertControllerWithTitle: (NSString*)            title
                          withMessage: (NSString*)            message
              presentedFromController: (UIViewController*)    controller
                       withCompletion: (AlertCompletionBlock) completion;


/**
 @author Nikolay Chaban
 
 Method designed for presenting simple inform alert with one confirm button

 @param title alert title string value
 @param message message inside alert string value
 @param source controller which will present alert controller
 */
+ (void) showInformAlertControllerWithTitle: (NSString*)         title
                                withMessage: (NSString*)         message
                             fromController: (UIViewController*) source;

/**
 @author Nikolay Chaban
 
 Method designed for presenting simple inform alert with one confirm button on top view controller
 
 @param title alert title string value
 @param message message inside alert string value
 */
+ (void) showOnTopInformAlertControllerWithTitle: (NSString*) title
                                     withMessage: (NSString*) message;

/**
 @author Gleb Cherkashyn
 
 Same method as showInformAlertControllerWithTitle:withMessage:fromController but with completion block
 */
+ (void) showInformAlertControllerWithTitle: (NSString*)             title
                                withMessage: (NSString*)             message
                             fromController: (UIViewController*)     source
                             withCompletion: (CompletionWithSuccess) completion;

/**
 @author Nikolay Chaban
 
 Methods designed for presenting alert controller with custom actions

 @param title alert title string
 @param message alert message string
 @param titles alert actions titles array
 @param controller controller which will present alert controller
 @param completion completion block with pressed action index
 */
+ (void) showAlertControllerWithTitle: (NSString*)                   title
                          withMessage: (NSString*)                   message
                    withActionsTitles: (NSArray*)                    titles
              presentedFromController: (UIViewController*)           controller
                  withCompletionBlock: (AlertActionsCompletionBlock) completion;

/**
 @author Valeria Mozghova
 
 Method for creating aler with text field and custom actions

 @param title - title for alert
 @param message - message in alert
 @param titles - array of titles for actions
 @param placeholderText - placeholder text for textfield
 @param controller - controller that presents alert
 @param completion - completion block with pressed action and entered text in text field
 */
+ (void) showAlertWithTextFieldWithTitle: (NSString*)                           title
                             withMessage: (NSString*)                           message
                       withActionsTitles: (NSArray*)                            titles
                     withPlaceholderText: (NSString*)                           placeholderText
                 presentedFromController: (UIViewController*)                   controller
                     withCompletionBlock: (AlertActionsWithTextCompletionBlock) completion;

+ (void) showActionSheetWithTitle: (NSString*)                   title
                          message: (NSString*)                   message
                withActionsTitles: (NSArray*)                    titles
          presentedFromController: (UIViewController*)           controller
              withCompletionBlock: (AlertActionsCompletionBlock) completion;
@end

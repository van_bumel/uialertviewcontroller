//
//  UIAlertController+Window.h
//  AviationDocs
//
//  Created by Nikolay Chaban on 10/10/17.
//  Copyright © 2017 Nikolay Chaban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Window)

- (void) show;

- (void) show: (BOOL) animated;

@end

//
//  AppDelegate.h
//  UIAlertViewController
//
//  Created by OnSight MacBook Pro on 6/7/18.
//  Copyright © 2018 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


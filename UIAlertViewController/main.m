//
//  main.m
//  UIAlertViewController
//
//  Created by OnSight MacBook Pro on 6/7/18.
//  Copyright © 2018 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

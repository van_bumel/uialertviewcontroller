//
//  ConstantsBlock.h
//  AviationDocs
//
//  Created by OnSight MacBook Pro on 6/7/18.
//  Copyright © 2017 Nikolay Chaban. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@protocol ConstantsBlock <NSObject>

typedef void(^CompletionWithSuccess)(BOOL isSuccess);

typedef void(^CompletionWithSuccessOrError)(BOOL isSuccess, NSError * _Nullable error);

typedef void(^ProgressOperationHandlerWithCompletion)(CGFloat progress, BOOL isSuccess, NSError* _Nullable error);

typedef void(^UpdateTabStateCompletionBlock)(BOOL isSuccess, NSUInteger pageNumber);

@end
